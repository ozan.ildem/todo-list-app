package com.ToDoList.ToDoApp.service;

import com.ToDoList.ToDoApp.entity.ListItem;
import com.ToDoList.ToDoApp.repo.ListItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ListItemService {
    private final ListItemRepo listItemRepo;

    @Autowired
    public ListItemService(ListItemRepo listItemRepo){
        this.listItemRepo = listItemRepo;
    }

    public ListItem addListItem(ListItem listItem){
        return listItemRepo.save(listItem);
    }

    public List<ListItem> getAllListItems(){
        return listItemRepo.findAll();
    }

    public void deleteAllListItems(){
        listItemRepo.deleteAll();
    }

    public ListItem findListItemById(Long id){
        return listItemRepo.findListItemById(id).orElseThrow(() -> new NoSuchElementException("no such List item exists"));
    }

    public ListItem toggleListItem(Long id){
        ListItem listItem = listItemRepo.findListItemById(id).orElseThrow(() -> new NoSuchElementException("no such List item exists"));
        listItem.setStatus(!listItem.getStatus());
        return listItemRepo.save(listItem);
    }
}
