package com.ToDoList.ToDoApp.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ListItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private long id;
    private String content;
    private Boolean status;

    public ListItem(){}

    public ListItem(String content, Boolean status) {
        this.content = content;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return "List item { " + "content = " + content + ", status = " + status + ", id = " +  id + " }";
    }
}
