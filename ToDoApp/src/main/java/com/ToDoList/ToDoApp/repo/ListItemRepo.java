package com.ToDoList.ToDoApp.repo;

import com.ToDoList.ToDoApp.entity.ListItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ListItemRepo extends JpaRepository<ListItem, Long> {

    Optional<ListItem> findListItemById(Long id);
}
