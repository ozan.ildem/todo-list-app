package com.ToDoList.ToDoApp.controller;

import com.ToDoList.ToDoApp.entity.ListItem;
import com.ToDoList.ToDoApp.service.ListItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ListItemController {
    private final ListItemService listItemService;

    public ListItemController(ListItemService listItemService) {
        this.listItemService = listItemService;
    }

    @GetMapping("/")
    public ResponseEntity<List<ListItem>>getAllListItems(){
        List<ListItem> list = listItemService.getAllListItems();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PutMapping("/toggle/{id}")
    public ResponseEntity<ListItem> toggleListItem(@PathVariable("id") Long id){
        ListItem listItem = listItemService.toggleListItem(id);
        return new ResponseEntity<>(listItem, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<ListItem> addListItem(@RequestBody ListItem listItem){
        ListItem newListItem = listItemService.addListItem(listItem);
        return new ResponseEntity<>(newListItem, HttpStatus.CREATED);
    }

    @DeleteMapping("/all")
    public ResponseEntity<?>deleteAllListItems(){
        listItemService.deleteAllListItems();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
