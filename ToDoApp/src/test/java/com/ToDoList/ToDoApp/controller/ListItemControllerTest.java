package com.ToDoList.ToDoApp.controller;

import com.ToDoList.ToDoApp.entity.ListItem;
import com.ToDoList.ToDoApp.service.ListItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ListItemController.class)
public class ListItemControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ListItemService service;

    @Test
    public void givenListItems_whenGetAllListItems_thenReturnJsonArray() throws Exception {
        ListItem listItem = new ListItem("einkaufen", false);
        List<ListItem> allListItems = asList(listItem);
        given(service.getAllListItems()).willReturn(allListItems);

        mvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].content", is(listItem.getContent())));
    }

    @Test
    public void whenDeleteAllListItems_thenReturnEmptyJsonArray() throws Exception{
        given(service.getAllListItems()).willReturn(Collections.emptyList());

        mvc.perform(delete("/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
