package com.ToDoList.ToDoApp.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ListItemTest {

    private ListItem item;

    @Before
    public void setUp(){
        item = new ListItem();
    }

    @Test
    public void generateListItemContentTest(){
        item.setContent("einkaufen");
        assertEquals("einkaufen", item.getContent());
    }

    @Test
    public void generateListItemStatusTest(){
        item.setStatus(true);
        assertEquals(true, item.getStatus());
    }

    @Test
    public void generateListItemID(){
        item.setId(123456L);
        assertEquals(123456L, item.getId());
    }

    @Test
    public void ListItemToStringTest(){
        item.setId(654321L);
        item.setContent("joggen");
        item.setStatus(false);
        assertEquals("List item { content = joggen, status = false, id = 654321 }", item.toString());
    }
}